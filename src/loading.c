/* loading.c generated by valac, the Vala compiler
 * generated from loading.vala, do not modify */

/* main.vala - This file is part of the psbrowser program
 *
 * Copyright (C) 2010  Lincoln de Sousa <lincoln@comum.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>


#define PS_BROWSER_UI_TYPE_LOADING (ps_browser_ui_loading_get_type ())
#define PS_BROWSER_UI_LOADING(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), PS_BROWSER_UI_TYPE_LOADING, PsBrowserUILoading))
#define PS_BROWSER_UI_LOADING_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), PS_BROWSER_UI_TYPE_LOADING, PsBrowserUILoadingClass))
#define PS_BROWSER_UI_IS_LOADING(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), PS_BROWSER_UI_TYPE_LOADING))
#define PS_BROWSER_UI_IS_LOADING_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), PS_BROWSER_UI_TYPE_LOADING))
#define PS_BROWSER_UI_LOADING_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), PS_BROWSER_UI_TYPE_LOADING, PsBrowserUILoadingClass))

typedef struct _PsBrowserUILoading PsBrowserUILoading;
typedef struct _PsBrowserUILoadingClass PsBrowserUILoadingClass;
typedef struct _PsBrowserUILoadingPrivate PsBrowserUILoadingPrivate;

struct _PsBrowserUILoading {
	GtkImage parent_instance;
	PsBrowserUILoadingPrivate * priv;
};

struct _PsBrowserUILoadingClass {
	GtkImageClass parent_class;
};

struct _PsBrowserUILoadingPrivate {
	gint _is_loading;
};


static gpointer ps_browser_ui_loading_parent_class = NULL;

GType ps_browser_ui_loading_get_type (void) G_GNUC_CONST;
#define PS_BROWSER_UI_LOADING_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), PS_BROWSER_UI_TYPE_LOADING, PsBrowserUILoadingPrivate))
enum  {
	PS_BROWSER_UI_LOADING_DUMMY_PROPERTY
};
PsBrowserUILoading* ps_browser_ui_loading_new (void);
PsBrowserUILoading* ps_browser_ui_loading_construct (GType object_type);
static gboolean _lambda0_ (PsBrowserUILoading* self);
static gboolean __lambda0__gsource_func (gpointer self);
static void _ps_browser_ui_loading_handle_loading (PsBrowserUILoading* self);
void ps_browser_ui_loading_ref_loading (PsBrowserUILoading* self);
void ps_browser_ui_loading_unref_loading (PsBrowserUILoading* self);
static void ps_browser_ui_loading_finalize (GObject* obj);



PsBrowserUILoading* ps_browser_ui_loading_construct (GType object_type) {
	PsBrowserUILoading * self;
	self = g_object_newv (object_type, 0, NULL);
	gtk_image_set_from_file ((GtkImage*) self, "data/pixmaps/loading.gif");
	return self;
}


PsBrowserUILoading* ps_browser_ui_loading_new (void) {
	return ps_browser_ui_loading_construct (PS_BROWSER_UI_TYPE_LOADING);
}


static gboolean _lambda0_ (PsBrowserUILoading* self) {
	gboolean result = FALSE;
	gtk_widget_set_visible ((GtkWidget*) self, self->priv->_is_loading > 0);
	result = FALSE;
	return result;
}


static gboolean __lambda0__gsource_func (gpointer self) {
	gboolean result;
	result = _lambda0_ (self);
	return result;
}


/**
 * Changes the visibility property of our `loading' image
 * based on the `self.is_loading' flag. This is important
 * because more than one action can request to show/hide it at
 * the same time.
 */
static void _ps_browser_ui_loading_handle_loading (PsBrowserUILoading* self) {
	g_return_if_fail (self != NULL);
	g_idle_add_full (G_PRIORITY_DEFAULT_IDLE, __lambda0__gsource_func, g_object_ref (self), g_object_unref);
}


/**
 * Increment the loading request and call the loading
 * show/hide function.
 */
void ps_browser_ui_loading_ref_loading (PsBrowserUILoading* self) {
	g_return_if_fail (self != NULL);
	self->priv->_is_loading++;
	_ps_browser_ui_loading_handle_loading (self);
}


/**
 * Decrement the loading request and call the loading
 * show/hide function.
 */
void ps_browser_ui_loading_unref_loading (PsBrowserUILoading* self) {
	g_return_if_fail (self != NULL);
	self->priv->_is_loading = MAX (0, self->priv->_is_loading - 1);
	_ps_browser_ui_loading_handle_loading (self);
}


static void ps_browser_ui_loading_class_init (PsBrowserUILoadingClass * klass) {
	ps_browser_ui_loading_parent_class = g_type_class_peek_parent (klass);
	g_type_class_add_private (klass, sizeof (PsBrowserUILoadingPrivate));
	G_OBJECT_CLASS (klass)->finalize = ps_browser_ui_loading_finalize;
}


static void ps_browser_ui_loading_instance_init (PsBrowserUILoading * self) {
	self->priv = PS_BROWSER_UI_LOADING_GET_PRIVATE (self);
	self->priv->_is_loading = 0;
}


static void ps_browser_ui_loading_finalize (GObject* obj) {
	PsBrowserUILoading * self;
	self = PS_BROWSER_UI_LOADING (obj);
	G_OBJECT_CLASS (ps_browser_ui_loading_parent_class)->finalize (obj);
}


/**
 * An abstraction for a loading widget that is shown when some async
 * job is running. You can use the .{un,}ref_loading() methods to ask
 * for showing or hidding it.
 */
GType ps_browser_ui_loading_get_type (void) {
	static volatile gsize ps_browser_ui_loading_type_id__volatile = 0;
	if (g_once_init_enter (&ps_browser_ui_loading_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (PsBrowserUILoadingClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) ps_browser_ui_loading_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (PsBrowserUILoading), 0, (GInstanceInitFunc) ps_browser_ui_loading_instance_init, NULL };
		GType ps_browser_ui_loading_type_id;
		ps_browser_ui_loading_type_id = g_type_register_static (GTK_TYPE_IMAGE, "PsBrowserUILoading", &g_define_type_info, 0);
		g_once_init_leave (&ps_browser_ui_loading_type_id__volatile, ps_browser_ui_loading_type_id);
	}
	return ps_browser_ui_loading_type_id__volatile;
}




